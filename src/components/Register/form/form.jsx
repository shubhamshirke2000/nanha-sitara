import React from "react"
import "../register.css"
import CreatableSelect from "react-select/creatable"
import { Error, Center, InputField } from "./styles"
import { Button, Input } from "../../common"
import { FastField, ErrorMessage } from "formik"
import MySelect from "../../../module/select-module"

export function Register(props) {
  var {
    values: { name, email, mobileNumber, about, skills },
    errors,
    touched,
    handleSubmit,
    handleChange,
    isValid,
    setFieldTouched,
    setFieldValue,
    isSubmitting,
  } = props

  const change = (name, e) => {
    handleChange(e)
    setFieldTouched(name, true, false)
  }

  return (
    <React.Fragment>
      <section
        class="contact-section section_padding"
        style={{ paddingTop: "120px" }}
      >
        <div class="container form-container">
          <div class="row">
            <div class="col-12">
              <h2 class="contact-title">Become a Volunteer</h2>
            </div>
            <div class="col-lg-8">
              <form
                class="form-contact contact_form"
                id="contactForm"
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <input
                        class="form-control"
                        id="name"
                        type="text"
                        as={FastField}
                        name="name"
                        aria-label="name"
                        placeholder="Full name*"
                        error={touched.name && errors.name}
                        value={name}
                        onChange={change.bind(null, "name")}
                      />
                      <ErrorMessage component={Error} name="name" />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input
                        class="form-control"
                        id="email"
                        aria-label="email"
                        as={FastField}
                        type="email"
                        name="email"
                        placeholder="Email*"
                        error={touched.email && errors.email}
                        value={email}
                        onChange={change.bind(null, "email")}
                      />{" "}
                      <ErrorMessage component={Error} name="email" />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input
                        class="form-control"
                        id="mobileNumber"
                        aria-label="mobileNumber"
                        as={FastField}
                        type="mobileNumber"
                        name="mobileNumber"
                        placeholder="Mobile number*"
                        error={touched.mobileNumber && errors.mobileNumber}
                        value={mobileNumber}
                        onChange={change.bind(null, "mobileNumber")}
                      />{" "}
                      <ErrorMessage component={Error} name="mobileNumber" />
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <textarea
                        class="form-control w-100"
                        as={FastField}
                        component="textarea"
                        aria-label="about"
                        id="about"
                        rows="8"
                        type="text"
                        name="about"
                        placeholder="About*"
                        error={touched.about && errors.about}
                        value={about}
                        onChange={change.bind(null, "about")}
                      ></textarea>{" "}
                      <ErrorMessage component={Error} name="about" />
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <MySelect
                        value={skills}
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                        error={errors.skills}
                        touched={touched.skills}
                      />
                    </div>
                  </div>
                </div>

                <div class="form-group mt-3">
                  <Button type="submit" disabled={!isValid || isSubmitting}>
                    Send Message <i class="flaticon-right-arrow"></i>{" "}
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </React.Fragment>
  )
}
