import Select from "react-select"
import CreatableSelect from "react-select/creatable"
import React from "react"

const options = [
  { value: "Reading", label: "Reading" },
  { value: "Writing", label: "Writing" },
  { value: "Speaking", label: "Speaking" },
  { value: "Thinking", label: "Thinking" },
  { value: "Creative", label: "Creative" },
  { value: "Drawing", label: "Drawing" },
]
class MySelect extends React.Component {
  handleChange = value => {
    // this is going to call setFieldValue and manually update values.topcis
    this.props.onChange("skills", value)
  }

  handleBlur = () => {
    // this is going to call setFieldTouched and manually update touched.topcis
    this.props.onBlur("skills", true)
  }

  render() {
    return (
      <div style={{ margin: "1rem 0" }}>
        <CreatableSelect
          id="color"
          isMulti
          options={options}
          multi={true}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          value={this.props.value}
        />
        {!!this.props.error && this.props.touched && (
          <div style={{ color: "red", marginTop: ".5rem" }}>
            {this.props.error}
          </div>
        )}
      </div>
    )
  }
}
export default MySelect
