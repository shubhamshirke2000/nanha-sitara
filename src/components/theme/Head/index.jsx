import React, { useState } from "react"
import Navbar from "./Navbar"
import Hamburger from "./Hamburger"
import Sidebar from "./Sidebar"
import { Wrapper, Overlay } from "./styles"
import "./header.css"
import $ from "jquery"

export const Headers = () => {
  const [sidebar, toggle] = useState(false)

  if (typeof window !== "undefined") {
    $(window).scroll(() => {
      if ($(window).scrollTop() >= 30) {
        $(".header").addClass("shadowHeader")
      } else {
        $(".header").removeClass("shadowHeader")
      }
    })
  }

  return (
    <Wrapper className="header">
      <Overlay sidebar={sidebar} onClick={() => toggle(!sidebar)} />
      <Navbar />
      <Hamburger sidebar={sidebar} toggle={toggle} />
      <Sidebar sidebar={sidebar} toggle={toggle} />
    </Wrapper>
  )
}
