import styled from "styled-components"

export const Button = styled.button`
  cursor: pointer;
  margin: auto;
  ${"" /* display: table; */}
  border-radius: 2rem;
  padding: 0.7rem 2.5rem;
  border: none;
  -webkit-appearance: none;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  color: #fff;
  background: #ff4800;
  @media (max-width: 680px) {
    margin: 1rem auto;
    display: table;
  }
  &:focus {
    outline: none;
  }

  &:disabled {
    background: gray;
  }

  ${({ secondary }) =>
    secondary &&
    `
		background: #001F3F;
	`}
`
