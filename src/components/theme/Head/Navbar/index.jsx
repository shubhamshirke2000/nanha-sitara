import React from "react"
import { Link } from "gatsby"
import { Container, Navbar as Nav } from "Common"
import NavbarLinks from "../NavbarLinks"
import { Wrapper } from "./styles"
import logo from "../../../../static/svg/logo.svg"

const Navbar = () => (
  <Wrapper as={Nav}>
    <Link to="/">
      <img
        src={logo}
        title="10x Growth Partners in thinking and execution of your ventures"
        alt="10xGrowth"
        style={{ height: "5.5rem" }}
      />
    </Link>
    <NavbarLinks desktop />
  </Wrapper>
)

export default Navbar
