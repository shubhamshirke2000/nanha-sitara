import React, { useState } from 'react'
import Recaptcha from 'react-google-recaptcha'
import axios from 'axios'
import { Button, Input } from '../../../common'
import {
  Error,
  Center,
  InputField,
} from '../../../landing/Contact/ContactForm/styles'
import { Contact10x } from '../../../../static/emailTemplates/contactUs'
import { FastField, ErrorMessage } from 'formik'

export function Form1(props) {
  const {
    values: { email },
    errors,
    touched,
    handleSubmit,
    handleChange,
    isValid,
    setFieldTouched,
    setFieldValue,
  } = props

  const change = (name, e) => {
    handleChange(e)
    setFieldTouched(name, true, false)
  }

  return (
    <React.Fragment>
      <form
        autoComplete="off"
        onSubmit={handleSubmit}
        id="Subscribe"
        className="subscribe-form"
      >
        <div className="form-cont">
          <p className="subc-input">
            <InputField>
              <input
                id="email-id"
                aria-label="email"
                component="input"
                as={FastField}
                type="email"
                className="subscribe-input"
                name="email"
                placeholder="Email*"
                error={touched.email && errors.email}
                value={email}
                onChange={change.bind(null, 'email')}
              />
              <ErrorMessage component={Error} name="email" />
            </InputField>
          </p>
          <p className="subc-button">
            <button
              secondary
              type="submit"
              className="subscribe-button"
              disabled={!isValid}
            >
              Submit
            </button>
          </p>
        </div>
      </form>
    </React.Fragment>
  )
}
