import React from 'react'
import { Form1 } from '../subscribeform/form'
import * as Yup from 'yup'
import { Formik } from 'formik'
import '../form.css'

const validationSchema = Yup.object({
  email: Yup.string()
    .email('Invalid email')
    .required('Email is required'),
})

export function FormContainer(props) {
  const handleSubmit = async (data, actions) => {
    try {
      //  var contactMessage = ContactForm1(data);
      //  await axios.post(`https://us-central1-orgz-app.cloudfunctions.net/api/contact`, {
      //   name: data.name,
      //   email: data.email,
      //   message: contactMessage
      // });

      let datavalue = {
        records: [
          {
            fields: {
              email: data.email,
              subscribedOn: Date.now(),
            },
          },
        ],
      }

      fetch(
        `https://api.airtable.com/v0/appjAjZqMljkN9OAm/newsLetter?api_key=keybD9vYsg2WIOj1W`,
        {
          method: 'POST',
          body: JSON.stringify(datavalue),
          headers: { 'Content-Type': 'application/json' },
        }
      )
        .then(res => {
          if (res.status === 200) {
            actions.resetForm()
            actions.setSubmitting(false)
            actions.setFieldValue('success', true)
          }
        })
        .catch(err => {
          actions.resetForm()
          actions.setSubmitting(false)
          actions.setFieldValue('success', false)
          alert('Something went wrong, please try again!')
        })
    } catch (err) {
      actions.resetForm()
      actions.setSubmitting(false)
      actions.setFieldValue('success', false)
      alert('Something went wrong, please try again!') // eslint-disable-line
    }
  }

  const values = {
    email: '',
  }

  return (
    <React.Fragment>
      <Formik
        render={props => <Form1 {...props} />}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
        initialValues={values}
      />
    </React.Fragment>
  )
}
