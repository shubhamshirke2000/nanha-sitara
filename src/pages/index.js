import React from "react"
import "../static/css/bootstrap.min.css"

import "../static/css/all.css"

import "../static/css/themify-icons.css"

import "../static/css/style.css"
import SEO from "../components/seo"
import Section1 from "../components/Section1"

import Section2 from "../components/Section2"
import Section3 from "../components/Section3"
import { Headers } from "../components/theme"
import { Footer } from "../components/theme/Foot"

const IndexPage = () => (
  <React.Fragment>
    <SEO title="Nanha Sitara" />

    <Headers />
    <Section1 />
    <Section3 />
    <Section2 />
    <Footer />
  </React.Fragment>
)

export default IndexPage
