import React from "react"
import "./section1.css"
import image from "../../static/image/banner_img.png"
import { Link } from "gatsby"
const Section1 = () => (
  <section class="banner_part">
    <div class="container">
      <div class="row align-items-center justify-content-between">
        <div class="col-lg-5">
          <div class="banner_img d-none d-lg-block">
            <img src={image} alt="" />
          </div>
        </div>
        <div class="col-lg-6">
          <div class="banner_text">
            <div class="banner_text_iner">
              <h1>Be Part Of Something More</h1>
              <p>
                "Give a man a fish and you feed him for a day. Teach a man to
                fish and you feed him for a lifetime."
              </p>
              <div className="btn-align">
                <Link to="/register">
                  <a href="#" class="btn_2">
                    Become a Volunteer
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
)
export default Section1
