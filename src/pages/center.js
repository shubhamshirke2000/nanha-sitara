import React from "react"
import "../static/css/bootstrap.min.css"
import "../static/css/all.css"
import "../static/css/themify-icons.css"
import "../static/css/style.css"
import SEO from "../components/seo"

import { Headers } from "../components/theme"
import { Footer } from "../components/theme/Foot"
import { RegisterContainer } from "../components/Register/container/handleForm"

const IndexPage = () => (
  <div style={{ background: " rgba(105, 105, 105, 0.05)" }}>
    <SEO title="Nanha Sitara - Centers" />

    <Headers />

    <RegisterContainer />
    <Footer />
  </div>
)

export default IndexPage
