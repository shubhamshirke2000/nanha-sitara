import React, { useEffect, useState } from "react"
import Avatar from "react-avatar"

export function VolunteerContainer(props) {
  const [flex, setFlex] = useState(false)
  console.log(props)
  useEffect(() => {
    if (props.all !== undefined) {
      setFlex(true)
    }
  })

  return (
    <React.Fragment>
      <div
        className="user-pro"
        style={flex ? { flexWrap: "wrap" } : { flexWrap: "nowrap" }}
      >
        {props.volunteers &&
          props.volunteers.map(volunteer =>
            props.all !== undefined ? (
              <div className="all-user-image">
                {volunteer.photo ? (
                  <Avatar
                    src={volunteer.photo}
                    name={volunteer.name}
                    className="avatar-size"
                    size="150px"
                  />
                ) : (
                  <Avatar
                    name={volunteer.name}
                    className="avatar-size"
                    size="150px"
                  />
                )}

                <h4>{volunteer.name}</h4>
              </div>
            ) : (
              <div className="user-image">
                {volunteer.photo ? (
                  <Avatar
                    src={volunteer.photo}
                    name={volunteer.name}
                    className="avatar-size"
                    size="150px"
                  />
                ) : (
                  <Avatar
                    name={volunteer.name}
                    className="avatar-size"
                    size="150px"
                  />
                )}

                <h4>{volunteer.name}</h4>
              </div>
            )
          )}
      </div>
    </React.Fragment>
  )
}
