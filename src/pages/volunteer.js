import React from "react"
import "../static/css/bootstrap.min.css"
import "../static/css/all.css"
import "../static/css/themify-icons.css"
import "../static/css/style.css"
import SEO from "../components/seo"

import AllVolunteers from "../components/AllVolunteer"
import { Footer } from "../components/theme/Foot"
import { Headers } from "../components/theme"
import Volunteer from "../components/Volunteer"

const IndexPage = () => (
  <div style={{ background: " rgba(246, 246, 246)" }}>
    <SEO title="Nanha Sitara - Volunteer" />

    <Headers />
    {/* <AllVolunteers /> */}
    <Volunteer />
    <Footer />
  </div>
)

export default IndexPage
