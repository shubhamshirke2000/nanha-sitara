import React from "react"
// import { Form1 } from "../subscribeform/form"
import * as Yup from "yup"
import { Formik } from "formik"
// import "../form.css"
import { Register } from "../form/form"
import { GetLocation } from "../../../module/getGeoLocation"

const validationSchema = Yup.object({
  name: Yup.string().required("Name is required"),
  email: Yup.string()
    .email("Invalid email")
    .required("Email is required"),
  mobileNumber: Yup.number().required("Mobile number is required"),
  about: Yup.string().required("About is required"),
  skills: Yup.array().of(
    Yup.object().shape({
      label: Yup.string().required(),
      value: Yup.string().required(),
    })
  ),
})

export function RegisterContainer(props) {
  var location = null

  const handleSubmit = async (data, actions) => {
    //  var contactMessage = ContactForm1(data);
    //  await axios.post(`https://us-central1-orgz-app.cloudfunctions.net/api/contact`, {
    //   name: data.name,
    //   email: data.email,
    //   message: contactMessage
    // });
    // GetLocation.then(res => {
    //   var values = {
    //     name: data.name,
    //     email: data.email,
    //     mobileNumber: data.mobileNumber,
    //     about: data.about,
    //     skills: JSON.stringify(data.skills),
    //     location: JSON.stringify(res),
    //   }
    //   try {
    //     let datavalue = {
    //       records: [
    //         {
    //           fields: {
    //             Name: values.name,
    //             Email: values.email,
    //             MobileNumber: values.mobileNumber,
    //             About: values.about,
    //             Skills: values.skills,
    //             Location: values.location,
    //           },
    //         },
    //       ],
    //     }

    //     fetch(
    //       `https://api.airtable.com/v0/appxrVfwTeIEMe8pl/Volunteers?api_key=keyeHojC1IyvdE7BA`,
    //       {
    //         method: "POST",
    //         body: JSON.stringify(datavalue),
    //         headers: { "Content-Type": "application/json" },
    //       }
    //     )
    //       .then(res => {
    //         if (res.status === 200) {
    //           actions.resetForm()
    //           actions.setSubmitting(false)
    //           actions.setFieldValue("success", true)

    //           alert("Success")
    //         }
    //       })
    //       .catch(err => {
    //         actions.resetForm()
    //         actions.setSubmitting(false)
    //         actions.setFieldValue("success", false)

    //         alert("Something went wrong, please try again!")
    //       })
    //   } catch (err) {
    //     actions.resetForm()
    //     actions.setSubmitting(false)
    //     actions.setFieldValue("success", false)

    //     alert("Something went wrong, please try again!") // eslint-disable-line
    //   }
    // })
    console.log(data)
    //
  }
  var values = {
    name: "",
    email: "",
    mobileNumber: "",
    about: "",
    skills: [],
    success: false,
  }

  return (
    <React.Fragment>
      <Formik
        render={props => <Register {...props} />}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
        initialValues={values}
      />
    </React.Fragment>
  )
}
