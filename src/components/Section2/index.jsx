import React, { useEffect, useState } from "react"
import "./section2.css"
import { GetVolunteers, GetContributor } from "../../module/getRequest"
import { VolunteerContainer } from "./container/volunteercontain"
import { Link } from "gatsby"

const Section2 = () => {
  var limit = 5
  if (typeof window !== undefined) {
    if (window.screen.width < 780) {
      limit = 3
    }
  }

  const [volunteers, setVolunteers] = useState([])
  useEffect(() => {
    getVolunteersInfo()
  }, [])

  // const getVolunteersInfo = () => {
  //   GetVolunteers.then(res => {
  //     res.map((volunteerData, index) => {
  //       if (limit === undefined) {
  //         setVolunteers(val => val.concat(volunteerData))
  //       } else if (index < limit) {
  //         setVolunteers(val => val.concat(volunteerData))
  //       }
  //     })
  //   })
  // }
  const getVolunteersInfo = () => {
    GetContributor.then(res => {
      res.map((volunteerData, index) => {
        if (limit === undefined) {
          setVolunteers(val => val.concat(volunteerData))
        } else if (index < limit) {
          setVolunteers(val => val.concat(volunteerData))
        }
      })
    })
  }

  return (
    <section className="about_us section_padding">
      <div className="container ">
        <div className="row align-items-center justify-content-between">
          <div className="col-md-12 col-lg-12">
            <div className="volunteer_title">
              <h2>Featured Volunteer</h2>
            </div>
            <div className="about_us_text">
              <VolunteerContainer volunteers={volunteers} />
            </div>
          </div>
          <div className="button-align">
            <Link to="/volunteers">
              <a href="#" class="btn_2">
                view more
              </a>
            </Link>
          </div>
          {/* <div className="col-md-6 col-lg-6">
          <div className="learning_img">
            <img src={img1} alt="" />
          </div>
        </div> */}
        </div>
      </div>
    </section>
  )
}
export default Section2
