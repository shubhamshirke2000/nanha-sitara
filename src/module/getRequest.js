import axios from "axios"
import { Airtable_key } from "../../data/airtable_key"

var GetVolunteers = new Promise(function(resolve, reject) {
  var volunteers = [],
    records = null

  axios
    .get("https://api.airtable.com/v0/appxrVfwTeIEMe8pl/Volunteers?", {
      headers: {
        Authorization: `Bearer ${Airtable_key}`,
      },
    })
    .then(res => {
      res.data.records &&
        res.data.records.map(data => {
          records = {
            id: `${data.fields.order}`,
            name: data.fields.Name,
            email: data.fields.Email,
            mobilenumber: data.fields.MobileNumber,
            volunteersId: data.id,
            about: data.fields.About,
            skills: data.fields.Skills,
            photo: data.fields.Photos[0].url,
          }
          volunteers.push(records)
        })

      resolve(volunteers)
    })
    .catch(err => {
      reject(err.message)
    })
})
var GetContributor = new Promise(function(resolve, reject) {
  var contributor = [],
    records = null

  axios
    .get(
      "https://api.airtable.com/v0/appKzdTSg4h37c0J5/Contributors?filterByFormula=AND(display)&sort%5B0%5D%5Bfield%5D=order",
      {
        headers: {
          Authorization: `Bearer ${Airtable_key}`,
        },
      }
    )
    .then(res => {
      res.data.records &&
        res.data.records.map(data => {
          records = {
            id: `${data.fields.order}`,
            name: data.fields.Name,
            contributorId: data.id,
            // photo: data.fields.Photos[0].url,
          }
          contributor.push(records)
        })

      resolve(contributor)
    })
    .catch(err => {
      reject(err.message)
    })
})
var GetCenters = new Promise(function(resolve, reject) {
  var centers = [],
    records = null

  axios
    .get("https://api.airtable.com/v0/appxrVfwTeIEMe8pl/Centers?", {
      headers: {
        Authorization: `Bearer ${Airtable_key}`,
      },
    })
    .then(res => {
      res.data.records &&
        res.data.records.map(data => {
          records = {
            // id: `${data.fields.order}`,
            branch: data.fields.Branch,

            centersId: data.id,

            view: data.fields.View[0].url,
          }
          centers.push(records)
        })

      resolve(centers)
    })
    .catch(err => {
      reject(err.message)
    })
})
var GetGrowthCenters = new Promise(function(resolve, reject) {
  var centers = [],
    records = null

  axios
    .get("https://api.airtable.com/v0/appCyujgBY0oN69DI/Centres?", {
      headers: {
        Authorization: `Bearer ${Airtable_key}`,
      },
    })
    .then(res => {
      res.data.records &&
        res.data.records.map(data => {
          records = {
            // id: `${data.fields.order}`,
            name: data.fields.Name,
            centersId: data.id,
            photo: data.fields.photo[0].url,
          }
          centers.push(records)
        })

      resolve(centers)
    })
    .catch(err => {
      reject(err.message)
    })
})

export { GetVolunteers, GetCenters, GetContributor, GetGrowthCenters }
