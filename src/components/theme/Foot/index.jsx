import React from "react"
import { Container } from "Common"
import { Wrapper, Flex, Links, Details } from "./styles"
import { Button, Input } from "../../common"
// import { Error, Center, InputField } from "../../landing/Contact/styles"
import social from "./social.json"
// import { FormContainer } from "./container/handleform"
import "./form.css"
export const Footer = () => (
  <Wrapper>
    <Flex as={Container}>
      <Details>
        <h2>Nanha Sitara</h2>
        <span>© All rights are reserved | {new Date().getFullYear()}</span>
      </Details>
      {/* <Links>
				{social && social.map(({ id, name, link, icon }) => (
					<a
						key={id}
						href={link}
						target="_blank"
						rel="noopener noreferrer"
						aria-label={`follow me on ${icon}`}
					>
						<img width="24" src={icon} alt={name} />
					</a>
				))}
			</Links> */}
      {/* <div className="subc-form">
        <p>Subscribe to our news letter.</p>
        <FormContainer />
      </div> */}
    </Flex>
  </Wrapper>
)
