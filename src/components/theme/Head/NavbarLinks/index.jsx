import React from "react"
import AnchorLink from "react-anchor-link-smooth-scroll"
import { Link } from "gatsby"
import { Wrapper } from "./styles"
import "./nav.css"

const NavbarLinks = ({ desktop }) => {
  return (
    <Wrapper desktop={desktop}>
      <Link to="/">
        <a>Home</a>
      </Link>
      {/* <AnchorLink href="#Clients">Clients</AnchorLink>
			 <AnchorLink href="#ContactUs">Contact</AnchorLink> */}
      <Link to="/">
        <a>Centers</a>
      </Link>
      <Link to="/volunteers">
        <a>Volunteers</a>
      </Link>
    </Wrapper>
  )
}

export default NavbarLinks
