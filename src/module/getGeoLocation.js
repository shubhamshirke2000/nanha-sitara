var GetLocation = new Promise(function(resolve, reject) {
  var records = null

  navigator.geolocation.getCurrentPosition(success, fail)
  function success(position) {
    records = {
      latitude: position.coords.latitude,
      longitude: position.coords.longitude,
    }

    resolve(records)
  }

  function fail(err) {
    reject(err)
  }
})

export { GetLocation }
