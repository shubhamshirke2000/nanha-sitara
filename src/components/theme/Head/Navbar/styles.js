import styled from "styled-components"

export const Wrapper = styled.div`
  padding: 3rem 0;
  display: flex;
  align-items: center;
  justify-content: space-between;

  a {
    color: #212121;
  }
  img {
    margin: auto;
    width: 12rem;
  }
`
