import React, { useEffect, useState } from "react"
import "./volunteers.css"
import { GetVolunteers, GetContributor } from "../../module/getRequest"
import { VolunteerContainer } from "../Section2/container/volunteercontain"
import { Link } from "gatsby"

const AllVolunteers = () => {
  const [volunteers, setVolunteers] = useState([])
  useEffect(() => {
    getVolunteersInfo()
  }, [])

  // const getVolunteersInfo = () => {
  //   GetVolunteers.then(res => {
  //     res.map((volunteerData, index) => {
  //       if (limit === undefined) {
  //         setVolunteers(val => val.concat(volunteerData))
  //       } else if (index < limit) {
  //         setVolunteers(val => val.concat(volunteerData))
  //       }
  //     })
  //   })
  // }
  const getVolunteersInfo = () => {
    GetContributor.then(res => {
      res.map((volunteerData, index) => {
        setVolunteers(val => val.concat(volunteerData))
      })
    })
  }

  return (
    <section
      className="about_us section_padding"
      style={{ paddingTop: "120px" }}
    >
      <div className="container ">
        <div className="row align-items-center justify-content-between">
          <div className="col-md-12 col-lg-12">
            <div className="volunteer_title1">
              <h2>Volunteers</h2>
            </div>
            <div className="about_us_text">
              <VolunteerContainer volunteers={volunteers} all={true} />
            </div>
          </div>

          {/* <div className="col-md-6 col-lg-6">
          <div className="learning_img">
            <img src={img1} alt="" />
          </div>
        </div> */}
        </div>
      </div>
    </section>
  )
}
export default AllVolunteers
