import React, { useEffect, useState } from "react"
import "./section3.css"
import Avatar from "react-avatar"
import { GetCenters, GetGrowthCenters } from "../../module/getRequest"
import { Link } from "gatsby"

const Section3 = () => {
  var limit = 5
  if (typeof window !== undefined) {
    if (window.screen.width < 780) {
      limit = 3
    }
  }

  const [centers, setCenters] = useState([])
  useEffect(() => {
    getCentersInfo()
  }, [])

  // const getCentersInfo = () => {
  //   GetCenters.then(res => {
  //     res.map((centerData, index) => {
  //       if (limit === undefined) {
  //         setCenters(val => val.concat(centerData))
  //       } else if (index < limit) {
  //         setCenters(val => val.concat(centerData))
  //       }
  //     })
  //   })
  // }
  const getCentersInfo = () => {
    GetGrowthCenters.then(res => {
      res.map((centerData, index) => {
        if (limit === undefined) {
          setCenters(val => val.concat(centerData))
        } else if (index < limit) {
          setCenters(val => val.concat(centerData))
        }
      })
    })
  }
  console.log(centers)
  return (
    <section className="about_us section_padding">
      <div className="container">
        <div className="row align-items-center justify-content-between">
          <div className="col-md-12 col-lg-12">
            <div className="center_title">
              <h2>Featured Center</h2>
            </div>
            <div className="about_us_text">
              <div className="center-pro">
                {centers &&
                  centers.map(center => (
                    <div className="center-image">
                      {center.photo ? (
                        <Avatar
                          src={center.photo}
                          name={center.name}
                          className="avatar-size1"
                          size="150px"
                        />
                      ) : (
                        <Avatar
                          name={center.name}
                          className="avatar-size1"
                          size="150px"
                        />
                      )}

                      <h4>{center.name}</h4>
                    </div>
                  ))}
              </div>
            </div>
          </div>
          <div className="button-align1">
            <Link to="/center">
              <a href="#" class="btn_2">
                Add Center
              </a>
            </Link>
          </div>
          {/* <div className="col-md-6 col-lg-6">
          <div className="learning_img">
            <img src={img1} alt="" />
          </div>
        </div> */}
        </div>
      </div>
    </section>
  )
}
export default Section3
