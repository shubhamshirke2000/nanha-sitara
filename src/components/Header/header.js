import PropTypes from "prop-types"
import React from "react"
import "../../static/css/bootstrap.min.css"
// import "../../static/css/animate.css"
// import "../../static/css/owl.carousel.min.css"
import "../../static/css/all.css"
// import "../../static/css/flaticon.css"
import "../../static/css/themify-icons.css"
// import "../../static/css/magnific-popup.css"
// import "../../static/css/slick.css"
import "../../static/css/style.css"

import logo from "../../static/image/logo.jpg"
import footerLogo from "../../static/img/footer_logo.png"

const Header = ({ siteTitle }) => (
  <header className="main_menu">
    <div className="container">
      <div className="row align-items-center">
        <div className="col-lg-12">
          <nav className="navbar navbar-expand-lg navbar-light">
            <a className="navbar-brand main_logo" href="index.html">
              {" "}
              <img src={logo} alt="logo" />{" "}
            </a>
            <a className="navbar-brand single_page_logo" href="index.html">
              {" "}
              <img src={footerLogo} alt="logo" />{" "}
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="menu_icon"></span>
            </button>

            <div
              className="collapse navbar-collapse main-menu-item"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav">
                <li className="nav-item">
                  <a className="nav-link" href="index.html">
                    Home
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="features.html">
                    features
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="pricing.html">
                    pricing
                  </a>
                </li>
                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="blog.html"
                    id="navbarDropdown"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Blog
                  </a>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="navbarDropdown"
                  >
                    <a className="dropdown-item" href="blog.html">
                      {" "}
                      blog
                    </a>
                    <a className="dropdown-item" href="single-blog.html">
                      Single blog
                    </a>
                  </div>
                </li>
                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="blog.html"
                    id="navbarDropdown1"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    pages
                  </a>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="navbarDropdown1"
                  >
                    <a className="dropdown-item" href="elements.html">
                      Elements
                    </a>
                  </div>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="contact.html">
                    Contact
                  </a>
                </li>
              </ul>
            </div>
            <a href="#" className="d-none d-sm-block btn_1 home_page_btn">
              sign up
            </a>
          </nav>
        </div>
      </div>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
